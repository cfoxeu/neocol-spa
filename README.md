# Neocol coding challenge - Stock take app

## git clone and navigate into the project directory

## Install dependencies
```sh
yarn
```

### Run the app from a dev server
```sh
yarn start
```

### Build the app
```sh
yarn build
```

### Host the app from a server (app must have been built)
```sh
yarn serve
```
