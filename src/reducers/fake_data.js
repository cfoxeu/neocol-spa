export default () => {
    return [
        { id: 0, name: 'Iron Cog', cost: 100, price: 120, shopCount: 127, warehouseCount: 873 },
        { id: 1, name: 'Plastic Tubing', cost: 2340, price: 2599, shopCount: 351, warehouseCount: 649 },
        { id: 2, name: 'Solar Panel', cost: 56300, price: 60100, shopCount: 8, warehouseCount: 56 },
        { id: 3, name: 'Tennis Ball', cost: 80, price: 250, shopCount: 60, warehouseCount: 440 }
    ]
}
