import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import fake_data from './fake_data'
import active_product from './active_product'
import stock_check from './stock_check'
import stock_check_active_product from './stock_check_active_product'

export default combineReducers({
  products: fake_data,
  activeProduct: active_product,
  stockCheck: stock_check,
  stockCheckActive: stock_check_active_product,
  routing: routerReducer
});
