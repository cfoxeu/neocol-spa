import React from 'react'

if(process.env.WEBPACK) require('./index.scss')

export default ({ title }) => (
    <div>
        <h1 className="app-title">{title}</h1>        
    </div>
);
