import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from '../home'
import NoMatch from '../nomatch'

import Header from '../header'

export default () => (
    <div>
        {/* <Header title="Neocol Stocktake App" /> */}
        <Switch>
            <Route exact path='/' component={Home} />
            <Route component={NoMatch} />
        </Switch>
    </div>
);
