import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import {
    selectProductStockCheck,
    addRow,
    calculateLoss
} from '../../actions/index'

if(process.env.WEBPACK) require('./index.scss')

const styles = {
    default: {
        display: 'none',
    },
    active: {
        display: 'block',
    }
}

class Stockcheck extends Component {
    createOptions() {
		return this.props.products.map(product => {
			return (
				<li
                    key={product.id}
                    onClick={() => this.props.selectProductStockCheck(product)}
                >
                    {product.name}
                </li>
			)
		})
	}
    getQuantity() {
        if (this.props.product) {
            if (this.props.stockCheck === 'shopfloor') {
                return this.props.product.shopCount
            }
            if (this.props.stockCheck === 'warehouse') {
                return this.props.product.warehouseCount
            } else {
                return 'Area type error'
            }
        } else {
            return 'Select a product'
        }
    }
    updateInput() {
        console.log('UPDATE')
    }
    render() {
        const {
            stockCheck,
            product
        } = this.props
        return (
            <div className="stock-check" style={stockCheck ? styles.active : styles.default}>
                <h2>Stock check for {stockCheck}</h2>
                <div className="input-group">
                    Product: {product ? product.name : 'None'}<br />
                    <ul className="product-list">
                        {this.createOptions()}
                    </ul>
                </div>
                <div className="input-group">
                    Quantity<br />
                    <input type="text" value={this.getQuantity()} onChange={this.updateInput()} placeholder="Enter new stock level..." />
                </div>
                <button onClick={() => {
                    this.props.addRow()
                }}>
                    + Add a row
                </button>
                <button onClick={() => {
                    this.props.calculateLoss()
                }}>
                    > Calculate loss
                </button>
            </div>
        )
    }
}

function mapStateToProps(state) {
	return {
        stockCheck: state.stockCheck,
        product: state.stockCheckActive,
		products: state.products
	}
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        selectProductStockCheck: selectProductStockCheck,
        addRow: addRow,
        calculateLoss: calculateLoss
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(Stockcheck)
