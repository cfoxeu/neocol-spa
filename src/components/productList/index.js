import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { selectProduct } from '../../actions/index'

if(process.env.WEBPACK) require('./index.scss')

class ProductList extends Component {
    createListItems() {
		return this.props.products.map(product => {
			return (
				<li
                    key={product.id}
                    onClick={() => this.props.selectProduct(product)}
                >
                    {product.name}
                </li>
			)
		})
	}
    render() {
        return (
            <div className="product-list">
                <h2>Products</h2>
                <ul>
                    {this.createListItems()}
                </ul>
            </div>
        )
    }
}

function mapStateToProps(state) {
	return {
		products: state.products
	}
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({ selectProduct: selectProduct }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(ProductList)
