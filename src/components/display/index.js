import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { selectArea } from '../../actions/index'

if(process.env.WEBPACK) require('./index.scss')

class Display extends Component {
    render() {
        const currencyFix = (val) => {
            return (val / 100).toFixed(2)
        }
        if (this.props.product) {
            const {
                name,
                cost,
                price,
                shopCount,
                warehouseCount
            } = this.props.product
            const unitTotalShopCost = currencyFix(shopCount * cost)
            const unitTotalShopPrice = currencyFix(shopCount * price)
            const unitTotalWarehouseCost = currencyFix(warehouseCount * cost)
            const unitTotalWarehousePrice = currencyFix(warehouseCount * price)
            return (
                <div className="display">
                    <div className="shopfloor">
                        <h3>Shop floor</h3>
                        <p>Total cost of stock: £{unitTotalShopCost}</p>
                        <p>Total price of stock: £{unitTotalShopPrice}</p>
                        <button onClick={() => this.props.selectArea('shopfloor')}>Stock check</button>
                    </div>
                    <div className="warehouse">
                        <h3>Warehouse</h3>
                        <p>Total cost of stock: £{unitTotalWarehouseCost}</p>
                        <p>Total price of stock: £{unitTotalWarehousePrice}</p>
                        <button onClick={() => this.props.selectArea('warehouse')}>Stock check</button>
                    </div>
                    <div className="active-product">
                        <h3>Product: {name}</h3>
                        <p>Cost per unit: £{currencyFix(cost)}</p>
                        <p>Price per unit: £{currencyFix(price)}</p>
                    </div>
                </div>
            )
        } else {
            let shopFloorTotalCost = 0
            let shopFloorTotalPrice = 0
            let warehouseTotalCost = 0
            let warehouseTotalPrice = 0
            this.props.products.map(p => {
                shopFloorTotalCost += (p.shopCount * p.cost)
                shopFloorTotalPrice += (p.shopCount * p.price)
                warehouseTotalCost += (p.warehouseCount * p.cost)
                warehouseTotalPrice += (p.warehouseCount * p.price)
            })
            return (
                <div className="display">
                    <div className="shopfloor">
                        <h3>Shop floor</h3>
                        <p>Total cost of stock: £{currencyFix(shopFloorTotalCost)}</p>
                        <p>Total price of stock: £{currencyFix(shopFloorTotalPrice)}</p>
                        <button onClick={() => this.props.selectArea('shopfloor')}>Stock check</button>
                    </div>
                    <div className="warehouse">
                        <h3>Warehouse</h3>
                        <p>Total cost of stock: £{currencyFix(warehouseTotalCost)}</p>
                        <p>Total price of stock: £{currencyFix(warehouseTotalPrice)}</p>
                        <button onClick={() => this.props.selectArea('warehouse')}>Stock check</button>
                    </div>
                    <div className="active-product">
                        <h3>Product: None selected</h3>
                    </div>
                </div>
            )
        }
    }
}

function mapStateToProps(state) {
	return {
        stockCheck: state.stockCheck,
		product: state.activeProduct,
        products: state.products
	}
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({ selectArea: selectArea }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(Display)
