import React, { Component } from 'react'

import ProductList from '../productList'
import Display from '../display'
import Stockcheck from '../stockCheck'

if(process.env.WEBPACK) require('./index.scss')

class Home extends Component {
	render() {
		return (
			<main className="home-page">
				<ProductList />
				<Display />
				<Stockcheck />
			</main>
		)
	}
}

export default Home
