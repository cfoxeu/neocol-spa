export const selectProduct = (product) => {
    return {
        type: 'PRODUCT_SELECTED',
        payload: product
    }
}
export const selectProductStockCheck = (product) => {
    return {
        type: 'STOCK_CHECK_PRODUCT_SELECTED',
        payload: product
    }
}
export const selectArea = (area) => {
    return {
        type: 'AREA_SELECTED',
        payload: area
    }
}
export const calculateLoss = (loss) => {
    return {
        type: 'CALCULATE_LOSS',
        payload: loss
    }
}
